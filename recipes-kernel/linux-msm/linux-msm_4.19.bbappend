# This recipe modifies the kernel build.

THISAPPENDFILESDIR := "${THISDIR}/files"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# Include fragment and build list of to merge
# To add more config fragments:
#   - Add the fragment file to the files directory
#   - Add the file name to the SRC_URI, as shown above
#   - Add the file path to the CFG_FRAGMENTS, as shown below

SRC_URI += "file://m005x.cfg"
CFG_FRAGMENTS = "${WORKDIR}/m005x.cfg "
COMMON_CFG = "${WORKDIR}/common-kona_defconfig"

# Include ModalAI patches
SRC_URI += "file://common-kona_defconfig"
SRC_URI += "file://001-kernel.patch"
SRC_URI += "file://003-qmi_wwan.patch"
SRC_URI += "file://004-option.patch"
SRC_URI += "file://004-qcserial.patch"
SRC_URI += "file://004-qmi_wwan.patch"
SRC_URI += "file://004-usb_wwan.patch"
SRC_URI += "file://005-option.patch"
SRC_URI += "file://005-qmi_wwan.patch"
SRC_URI += "file://006-dma-unmapping.patch"
SRC_URI += "file://006-subsystem-restart.patch"
SRC_URI += "file://0001-m0054-add.patch"
SRC_URI += "file://0001-m0052-add.patch"
SRC_URI += "file://m00xx-kona-coresight.dtsi"
SRC_URI += "file://m00xx-kona-audio.dtsi"
SRC_URI += "file://m00xx-kona-audio-overlay.dtsi"
SRC_URI += "file://m00xx-kona-bus.dtsi"
SRC_URI += "file://m00xx-kona-camera.dtsi"
SRC_URI += "file://m00xx-kona-cvp.dtsi"
SRC_URI += "file://m00xx-kona-gpu.dtsi"
SRC_URI += "file://m00xx-kona-ion.dtsi"
SRC_URI += "file://m00xx-kona-iot-rb5-audio.dtsi"
SRC_URI += "file://m00xx-kona-lpi.dtsi"
SRC_URI += "file://m00xx-kona-mhi.dtsi"
SRC_URI += "file://m00xx-kona-npu.dtsi"
SRC_URI += "file://m00xx-kona-pcie.dtsi"
SRC_URI += "file://m00xx-kona-pm.dtsi"
SRC_URI += "file://m00xx-kona-pmic-overlay.dtsi"
SRC_URI += "file://m00xx-kona-regulators.dtsi"
SRC_URI += "file://m00xx-kona-sde.dtsi"
SRC_URI += "file://m00xx-kona-sde-pll.dtsi"
SRC_URI += "file://m00xx-kona-smp2p.dtsi"
SRC_URI += "file://m00xx-kona-thermal.dtsi"
SRC_URI += "file://m00xx-kona-thermal-overlay.dtsi"
SRC_URI += "file://m00xx-kona-usb.dtsi"
SRC_URI += "file://m00xx-kona-v2.1-gpu.dtsi"
SRC_URI += "file://m00xx-kona-v2-gpu.dtsi"
SRC_URI += "file://m00xx-kona-va-bolero.dtsi"
SRC_URI += "file://m00xx-kona-vidc.dtsi"
SRC_URI += "file://m00xx-modalai-qupv3.dtsi"
SRC_URI += "file://m0052-qrb5165.dtsi"
SRC_URI += "file://m0052-kona-v2.dtsi"
SRC_URI += "file://m0052-kona.dtsi"
SRC_URI += "file://m0052-kona-v2.1-iot-rb5.dtsi"
SRC_URI += "file://m0052-kona-qrd.dtsi"
SRC_URI += "file://m0054-qrb5165.dtsi"
SRC_URI += "file://m0054-kona-v2.dtsi"
SRC_URI += "file://m0054-kona.dtsi"
SRC_URI += "file://m0054-kona-v2.1-iot-rb5.dtsi"
SRC_URI += "file://m0054-kona-qrd.dtsi"

patch() {
    cp ${COMMON_CFG} ${S}/arch/arm64/configs/vendor/m0052-kona_defconfig
    cp ${COMMON_CFG} ${S}/arch/arm64/configs/vendor/m0054-kona_defconfig
    cp ${WORKDIR}/m00xx-kona-coresight.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-coresight.dtsi
    cp ${WORKDIR}/m00xx-kona-audio.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-audio.dtsi
    cp ${WORKDIR}/m00xx-kona-audio-overlay.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-audio-overlay.dtsi
    cp ${WORKDIR}/m00xx-kona-bus.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-bus.dtsi
    cp ${WORKDIR}/m00xx-kona-cvp.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-cvp.dtsi
    cp ${WORKDIR}/m00xx-kona-gpu.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-gpu.dtsi
    cp ${WORKDIR}/m00xx-kona-ion.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-ion.dtsi
    cp ${WORKDIR}/m00xx-kona-iot-rb5-audio.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-iot-rb5-audio.dtsi
    cp ${WORKDIR}/m00xx-kona-lpi.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-lpi.dtsi
    cp ${WORKDIR}/m00xx-kona-mhi.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-mhi.dtsi
    cp ${WORKDIR}/m00xx-kona-npu.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-npu.dtsi
    cp ${WORKDIR}/m00xx-kona-pcie.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-pcie.dtsi
    cp ${WORKDIR}/m00xx-kona-pm.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-pm.dtsi
    cp ${WORKDIR}/m00xx-kona-pmic-overlay.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-pmic-overlay.dtsi
    cp ${WORKDIR}/m00xx-kona-regulators.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-regulators.dtsi
    cp ${WORKDIR}/m00xx-kona-sde.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-sde.dtsi
    cp ${WORKDIR}/m00xx-kona-sde-pll.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-sde-pll.dtsi
    cp ${WORKDIR}/m00xx-kona-smp2p.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-smp2p.dtsi
    cp ${WORKDIR}/m00xx-kona-thermal.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-thermal.dtsi
    cp ${WORKDIR}/m00xx-kona-thermal-overlay.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-thermal-overlay.dtsi
    cp ${WORKDIR}/m00xx-kona-usb.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-usb.dtsi
    cp ${WORKDIR}/m00xx-kona-v2.1-gpu.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-v2.1-gpu.dtsi
    cp ${WORKDIR}/m00xx-kona-v2-gpu.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-v2-gpu.dtsi
    cp ${WORKDIR}/m00xx-kona-va-bolero.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-va-bolero.dtsi
    cp ${WORKDIR}/m00xx-kona-vidc.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-kona-vidc.dtsi
    cp ${WORKDIR}/m00xx-modalai-qupv3.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m00xx-modalai-qupv3.dtsi
    cp ${WORKDIR}/m00xx-kona-camera.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/camera/m00xx-kona-camera.dtsi
    cp ${WORKDIR}/m0052-qrb5165.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-qrb5165.dtsi
    cp ${WORKDIR}/m0052-kona-v2.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-v2.dtsi
    cp ${WORKDIR}/m0052-kona.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona.dtsi
    cp ${WORKDIR}/m0052-kona-v2.1-iot-rb5.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-v2.1-iot-rb5.dtsi
    cp ${WORKDIR}/m0052-kona-qrd.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0052-kona-qrd.dtsi   
    cp ${WORKDIR}/m0054-qrb5165.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-qrb5165.dtsi
    cp ${WORKDIR}/m0054-kona-v2.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-v2.dtsi
    cp ${WORKDIR}/m0054-kona.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona.dtsi
    cp ${WORKDIR}/m0054-kona-v2.1-iot-rb5.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-v2.1-iot-rb5.dtsi
    cp ${WORKDIR}/m0054-kona-qrd.dtsi ${S}/arch/arm64/boot/dts/vendor/qcom/m0054-kona-qrd.dtsi    
}

do_patch_prepend() {
    bb.build.exec_func('patch', d)	
}

do_configure_prepend() {
   # We merge the config changes with the default config for the board
   # using merge-config.sh kernel tool

   mergeTool=${S}/scripts/kconfig/merge_config.sh
   confDir=${S}/arch/${ARCH}/configs
   defconf=${confDir}/${KERNEL_CONFIG}

   ${mergeTool} -m -O ${confDir} ${defconf} ${CFG_FRAGMENTS}

   # The output will be named .config. We rename it back to ${defconf} because
   # that's what the rest of do_configure expects
   mv ${confDir}/.config ${defconf}
   bbnote "Writing back the merged config: ${confDir}/.config to ${defconf}"
}

EXTRA_OEMAKE += "PLATFORM_TYPE=${MACHINE}"
